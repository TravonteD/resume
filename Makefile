resume.pdf: resume.typ
	typst compile $<

clean: $(wildcard *.log) $(wildcard *.aux)
	rm $^
