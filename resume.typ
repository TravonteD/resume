#let split_header(left_side, right_side) = {
  grid(
    columns: (1fr, 1fr),
    align(left)[
      #left_side
    ],
    align(right)[
      #parbreak()
      #right_side
    ]
  )
}

#set page(
  paper: "a4",
  margin: (x: 0.0in, y: 0.5in),
)
#set list(spacing: 1.5em)

#grid(
  columns: (1fr, 1fr),
  align(left, text(18pt)[
    *Travonte "Tray" Dennis*
  ]),
  align(right)[
    Atlanta, GA | 470.455.5787 | tray\@trayd.xyz
  ]
)

= Summary
Highly skilled and experienced software engineer with expertise in a wide range of programming languages including Ruby, Python, Elixir, JavaScript/TypeScript, Golang, Rust, Clojure, C\#, Java, SQL, PHP, and Shell. Proficient in both front-end and back-end development, with in-depth knowledge of frameworks such as React.js, Vue.js, Angularjs, JQuery, SolidJS, Ruby on Rails, and Flask. Extensive experience in testing frameworks like Cypress, RSpec, Jest, and Pytest. Skilled in utilizing various utilities such as Linux, UNIX-like systems, Virtual Machines, Docker, Kubernetes, RabbitMQ, Ansible, Apache, Nginx, AWS, GCP, and debugging tools. Strong track record of leading development teams, driving technical roadmaps, and delivering high-quality solutions.

= Skills
*Languages* ---
Ruby, Python, Elixir, JavaScript/Typescript, Golang, Rust, Clojure, C\#, Java, SQL, PHP, and Shell

*Front-End Frameworks* ---
React.js, Vue.js, Angularjs, JQuery, SolidJS

*Back-End Frameworks* ---
Ruby on Rails, Flask

*Testing Frameworks* ---
Cypress, RSpec, Jest, and Pytest

*Utilities* ---
Linux and other UNIX-like systems, Virtual Machines, Docker, Kubernetes, RabbitMQ, Ansible, Apache, Nginx, AWS, GCP and Debugging tools for the various languages utilized

= Experience

#split_header([
  *Grantboost AI* --- Atlanta, GA

  _Technical Co-Founder_
], [March 2023 --- Present])

- Develops and maintains full stack applications in Golang
- Integrates LLM model api for use within the application such as OpenAI, Azure AI
- Gathers and monitors metrics using dashboards built using Prometheus and Grafana
- Manages server resources and deployments using Linux, Docker, and Github Actions


#split_header([
  *Doximity* --- San Francisco, CA

  _Senior Software Engineer_
], [November 2023 --- December 2024])

- Developed software containing multiple services written with Ruby on Rails
- Developed dynamic and interactive front-end applications using Node.js, Vue.js, and GraphQL, enhancing user experience and performance
- Wrote and optimized Elasticsearch queries for both accuracy and performance with detailed testing
- Analyzed and triaged bugs and unexpected behavior
- Utilized AWS EC2 instances for deployment
- Contributed to software automation tools written in Go for internal use

#split_header(
  [
    *Salesloft.* --- Atlanta, GA

    _Senior Software Engineer - Engineering Enablement_
  ], [September 2020 --- November 2023]
)

- Lead planning on long-term development roadmaps, grooming, and sprint planning
- Dedicated time to training of associate and midlevel engineers through documentation, presentations and pairing sessions
- Developed internal tooling for engineer local development environments in NodeJS, Ruby, Elixir, Python and Golang
- Managed Kubernetes manifests for services hosted in AWS/GCP
- Developed internal tooling for development process automation
- Planned and implements language and tooling upgrades to maintain technical debt and security vulnerabilities
- Worked cross-team to implement organization-wide projects
- Jumped-in daily for ad-hoc unblocking team-members and stakeholders
- Engineered robust front-end solutions with React.js and Node.js delivering high-quality user interfaces and improved performance.
- Developed full-stack applications using Python, Flask, and SQLAlchemy

#split_header(
  [
    *OneTrust LLC.* --- Sandy Springs, GA

    _Web Developer_
  ], [October 2019 --- April 2020]
)

- Developed templating components for Wordpress using PHP, SCSS, and Javascript
- Created automated workflow solutions for the team to reduce repetitive tasks
- Utilized Cypress framework for end to end testing

#split_header(
  [
    *HERE Technologies* --- Alpharetta, GA

    _Full Stack Web Developer_
  ], [February 2018 --- October 2019]
)

- Utilized AWS EC2 instances for deployment of external facing services
- Developed solutions for automating network monitoring and automation
- Assisted in maintenance of 12 virtual machines used in various environments by the team using Ansible
- Deployed containerized projects to the docker swarm and provides trouble shooting in the event of an incident
- Developed complex front-ends using Vue.js
- Maintained the back-end of several projects built in Ruby, including two internal Ruby gems
- Supported internal applications with API integrations
- Utilized RabbitMQ message bus framework to implement asynchronous API request processing
- Assisted in training of interns and participates heavily in code and process review for the team

#split_header(
  [
    *HERE Technologies* --- Alpharetta, GA

    _Full Stack Web Developer Intern_
  ], [August 2017 --- January 2018]
)

- Developed an API for a network device database using Rails
- Designed and built two network tools using Rails, Bootstrap, and JQuery Datatables
- Wrote test specifications for Rails applications using Rspec

= Education

#split_header(
  [
    *Year Up* --- Atlanta, GA

    _Software Development_
  ], [March 2017 --- August 2017]
)
- Accrue 200+ hours of hands-on training in front-end web development and quality assurance
- Earning 18+ college credits towards a computer science degree in partnership with Atlanta Technical College
- Relevant courses include: Business Writing, Public Speaking, Computer Applications, Scripting languages, Database Management, and Intro to Web Design.

#split_header(
  [
    *Kennesaw State University* --- Marietta, GA

    _Software Engineering_
  ], [August 2015 --- May 2016]
)
- Relevant Courses: Java Programming 1, Java Programming 2,and Calculus
